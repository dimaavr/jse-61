package ru.tsc.avramenko.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.avramenko.tm.model.AbstractEntity;

public interface Repository<E extends AbstractEntity> extends JpaRepository<E, String> {

}