package ru.tsc.avramenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.avramenko.tm.dto.SessionDTO;

import java.util.List;

public interface SessionDtoRepository extends DtoRepository<SessionDTO> {

    @Query("SELECT e FROM SessionDTO e WHERE e.userId = :userId")
    @Nullable List<SessionDTO> findAllById(@Param("userId") @NotNull String userId);

    @Modifying
    @Query("DELETE FROM SessionDTO e")
    void clear();

    @Modifying
    @Query("DELETE FROM SessionDTO e WHERE e.userId = :userId")
    void clear(@Param("userId") @NotNull String userId);

    @Query("SELECT e FROM SessionDTO e WHERE e.id = :id")
    @Nullable SessionDTO findSessionById(@Param("id") @NotNull String id);

}