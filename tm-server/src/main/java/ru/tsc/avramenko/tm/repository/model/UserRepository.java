package ru.tsc.avramenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.avramenko.tm.model.User;

public interface UserRepository extends Repository<User> {

    @Query("SELECT e FROM User e WHERE e.id = :id")
    @Nullable User findUserById(
            @Param("id") @NotNull String id
    );

    @Modifying
    @Query("DELETE FROM User e")
    void clear();

    @Query("SELECT e FROM User e WHERE e.login = :login")
    @Nullable User findByLogin(
            @Param("login") @NotNull String login
    );

    @Query("SELECT e FROM User e WHERE e.email = :email")
    @Nullable User findByEmail(
            @Param("email") @NotNull String email
    );

    @Modifying
    @Query("DELETE FROM User e WHERE e.login = :login")
    void removeUserByLogin(
            @Param("login") @NotNull String login
    );

}