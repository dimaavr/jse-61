package ru.tsc.avramenko.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.avramenko.tm.dto.AbstractEntityDTO;

public interface DtoRepository<E extends AbstractEntityDTO> extends JpaRepository<E, String> {

}