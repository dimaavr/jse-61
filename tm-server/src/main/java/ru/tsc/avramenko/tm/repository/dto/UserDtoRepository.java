package ru.tsc.avramenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.avramenko.tm.dto.UserDTO;

public interface UserDtoRepository extends DtoRepository<UserDTO> {

    @Modifying
    @Query("DELETE FROM UserDTO e")
    void clear();

    @Query("SELECT e FROM UserDTO e WHERE e.login = :login")
    @Nullable UserDTO findByLogin(
            @Param("login") @NotNull String login
    );

    @Query("SELECT e FROM UserDTO e WHERE e.id = :id")
    @Nullable UserDTO findUserById(
            @Param("id") @NotNull String id
    );

    @Query("SELECT e FROM UserDTO e WHERE e.email = :email")
    @Nullable UserDTO findByEmail(
            @Param("email") @NotNull String email
    );

    @Modifying
    @Query("DELETE FROM UserDTO e WHERE e.login = :login")
    void removeUserByLogin(
            @Param("login") @NotNull String login
    );

}