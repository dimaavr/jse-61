package ru.tsc.avramenko.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.listener.AbstractListener;
import ru.tsc.avramenko.tm.event.ConsoleEvent;

@Component
public class AboutDisplayListener extends AbstractListener {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @Nullable
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer information.";
    }

    @Override
    @EventListener(condition = "@aboutDisplayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: " + Manifests.read("developer"));
        System.out.println("E-MAIL: " + Manifests.read("email"));
    }

}