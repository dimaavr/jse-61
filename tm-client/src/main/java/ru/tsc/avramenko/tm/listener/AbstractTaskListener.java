package ru.tsc.avramenko.tm.listener;

import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.endpoint.TaskDTO;

public abstract class AbstractTaskListener extends AbstractListener {

    protected void showTask(@Nullable TaskDTO task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().value());
        System.out.println("Project Id: " + task.getProjectId());
        System.out.println("Created: " + task.getCreated());
        System.out.println("Start Date: " + task.getStartDate());
        System.out.println("Finish Date: " + task.getFinishDate());
    }

}