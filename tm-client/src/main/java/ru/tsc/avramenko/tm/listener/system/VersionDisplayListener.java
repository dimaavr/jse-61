package ru.tsc.avramenko.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.listener.AbstractListener;
import ru.tsc.avramenko.tm.event.ConsoleEvent;

@Component
public class VersionDisplayListener extends AbstractListener {

    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @Nullable
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    @EventListener(condition = "@versionDisplayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println("PROGRAM VERSION: " + propertyService.getApplicationVersion());
        System.out.println("BUILD NUMBER: " + Manifests.read("build"));
    }

}