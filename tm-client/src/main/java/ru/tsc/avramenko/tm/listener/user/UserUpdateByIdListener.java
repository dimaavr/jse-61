package ru.tsc.avramenko.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.listener.AbstractUserListener;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class UserUpdateByIdListener extends AbstractUserListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "user-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user info by id.";
    }

    @Override
    @EventListener(condition = "@userUpdateByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER USER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final UserDTO user = adminUserEndpoint.findUserById(session, id);
        if (user == null) throw new UserNotFoundException();
        System.out.println("ENTER FIRST NAME: ");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        @Nullable final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        @Nullable final String email = TerminalUtil.nextLine();
        adminUserEndpoint.updateUserById(session, firstName, lastName, middleName, email);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}